module.exports = function(request, response, next){
  //je pense qu'il y a un autre moyen de faire fonctionner 
  //les request mais pour l'instant c'est le seul que j'ai trouver
  let req = require('request')
  
  //récupération de toute les races de chat
  request.getAllBreeds = function(callback){

    var headerCatAPI = {
        'method': 'GET',
        'url': 'https://api.thecatapi.com/v1/breeds',
        'headers': {
          'Content-Type': 'application/json',
          'api_key': '3ffef318-62b6-49e9-8607-46b792cca123'
        }
    };

    req(headerCatAPI, (error, res, body) => {
        return callback(body)
    })
  }
  //récupération de toute les catégories d'images de chat
  request.getAllCategories = function(callback){

    var headerCatAPI = {
        'method': 'GET',
        'url': 'https://api.thecatapi.com/v1/categories',
        'headers': {
          'Content-Type': 'application/json',
          'api_key': '3ffef318-62b6-49e9-8607-46b792cca123'
        }
    };

    req(headerCatAPI, (error, res, body) => {
        return callback(body)
    })
  }

  //return JSON parsable value
  request.getRandomPic = function(callback){

      var headerCatAPI = {
          'method': 'GET',
          'url': 'https://api.thecatapi.com/v1/images/search?limit=1',
          'headers': {
            'Content-Type': 'application/json',
            'api_key': '3ffef318-62b6-49e9-8607-46b792cca123'
          }
        };

      req(headerCatAPI, (error, res, body) => {
      return callback(body)
    })

  }

  request.getPic = function(pic_id, callback){

      var headerCatAPI = {
          'method': 'GET',
          'url': 'https://api.thecatapi.com/v1/images/'+pic_id,
          'headers': {
            'Content-Type': 'application/json',
            'api_key': '3ffef318-62b6-49e9-8607-46b792cca123'
          }
      };

      req(headerCatAPI, (error, res, body) => {
          return callback(body)
      })
  }

  request.getAllPics = function(page, breed, categorie, callback){

    reqUrl = 'https://api.thecatapi.com/v1/images/search?'
    if (typeof page !== 'undefined') {
      reqUrl += 'page='+page+'&'
    }
    if (typeof breed !== 'undefined' && breed != 'noBreed' && breed != 'undefined') {
      reqUrl += 'breed_ids='+breed+'&'
    }
    if (typeof categorie !== 'undefined' && categorie != 'noCategorie' && categorie != 'undefined') {
      reqUrl += 'category_ids='+categorie+'&'
    }
    reqUrl += 'limit=100'

    console.log(reqUrl)

    var headerCatAPI = {
        'method': 'GET',
        'url': reqUrl,
        'headers': {
          'Content-Type': 'application/json',
          'api_key': '3ffef318-62b6-49e9-8607-46b792cca123'
        }
    };

    req(headerCatAPI, (error, res, body) => {
        return callback(body)
    })
  }

  next()
}