//Crée la variable relative au serveur express
let express = require('express')
let req = require('request')
let bodyParser = require('body-parser')
let favicon = require('serve-favicon')
//Lance l'app qui permet de récupérer les changement d'état du server
let app = express()

//Initialise l'utilisation de ejs pour l'html
app.set('view engine', 'ejs')

//Rend static tout ce qui se trouve dans le dossier "public" ici
app.use('/assets', express.static('public'))
//Utilisation du middleware bodyParser qui permet la récupération des POST formulaire
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
//utilisation du chemin pour définir la favicon
app.use(favicon('./public/img/favicon.ico'));
//Utilisation de boostrap et jquery
app.use('/bootstrap', express.static('node_modules/bootstrap'))
app.use('/jquery', express.static('node_modules/jquery'))
//Utilisation d'un middleware pour gérer les function relative à l'API
app.use(require('./middlewares/catAPI'))



app.get('/', (request, response) => {

    response.render('pages/index', {test: "le passage de variable est ignoble"})
})
//renvoie vers une page 
app.get('/breeds', (request, response) => {

    //récupération de toute les races pour gérer le select de la librairie
    request.getAllBreeds( function(res2){
        allBreeds = JSON.parse(res2)
        response.render('pages/breeds', {listBreeds: allBreeds})
    })  
    
})
//revoie vers une bibliothèque de toute les images
app.get('/library/:page', (request, response) => {

    let searchTabl = {
        page: request.params.page,
        categorie: request.params.categorie,
        breed: request.params.breed
    }

    request.getAllPics(searchTabl['page'], searchTabl['breed'], searchTabl['categorie'], function(res1){     
        resJSON = JSON.parse(res1)

        //récupération de toute les races pour gérer le select de la librairie
        request.getAllBreeds( function(res2){
            allBreeds = JSON.parse(res2)

            //récupération de toute les races pour gérer le select de la librairie
            request.getAllCategories( function(res3){
                allCategories = JSON.parse(res3)
                response.render('pages/library', {searchInfo: searchTabl, 
                                                listPictures: resJSON,
                                                listBreeds: allBreeds,
                                                listCategories: allCategories})

            }) 
        })  
    })    
})

//doit y avoir plus propre pour passer les valeur du formulaire au get fait au dessus mais ce copier coller fonctionne bien
//permet de faire une selection en fonction des races ou catégorie des images dans la bibliothèque
app.post('/library/search', (request, response) => {

    breed = (typeof request.body.breed === 'undefined' ? undefined : request.body.breed) 
    categorie = (typeof request.body.categorie === 'undefined' ? undefined : request.body.categorie) 
    let searchTabl = {
        page: request.params.page,
        categorie: categorie,
        breed: breed
    }

    //récupération de toute les races pour gérer le select de la librairie
    request.getAllBreeds( function(res1){
        allBreeds = JSON.parse(res1)

        //récupération de toute les races pour gérer le select de la librairie
        request.getAllCategories( function(res2){
            allCategories = JSON.parse(res2)

            request.getAllPics(searchTabl['page'], searchTabl['breed'], searchTabl['categorie'], function(res3){     
                resJSON = JSON.parse(res3)

                response.render('pages/library', {searchInfo: searchTabl, 
                                                listPictures: resJSON,
                                                listBreeds: allBreeds,
                                                listCategories: allCategories})
            }) 
        })  
    })    
})

//renvoie une image aléatoire
app.get('/random', (request, response) => {

    request.getRandomPic( function(res){
        resJSON = JSON.parse(res)
        response.render('pages/index', {randomPic_url: resJSON[0].url, randomPic_id: resJSON[0].id})    
     
    })    
})

//test breeds : HW1MGAuGL
//test categorie : 5td
app.get('/info/:id', (request, response) => {
    request.getPic(request.params.id, function(res){
        resJSON = JSON.parse(res)
        response.render('pages/showPic', {raw_data: resJSON})    
    
    }) 
})


app.listen(8080)