Voici mon exercice sur NodeJS utilisant l'API Cat-API.

Pour faire fonctionner celui-ci en local il vous faut donc avoir installé NodeJS.

Voici les quelques étapes pour pouvoir lancer le serveur :

- Cloner le repo si présent
- Ouvrez un terminal et placer vous dans le dossier cloner dézippé
- lancer 'npm install'
- lancer "npm init" pour installé les dépenses nécessaires au fonctionnement (création du dossier node_modules)
- lancer "npm start" pour démarrer le serveur
- ouvrez votre navigateur en "localhost:8080"

Et maintenant Have Fun !
